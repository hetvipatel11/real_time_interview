
pip install dtw

pip install librosa

import librosa
import librosa.display
import matplotlib.pyplot as plt
from dtw import dtw
from scipy.spatial.distance import euclidean,mahalanobis

#Loading audio files
y1, sr1 = librosa.load('3.wav')
y2, sr2 = librosa.load('1.wav')

#Showing multiple plots using subplot
plt.subplot(2, 1, 2)
mfcc1 = librosa.feature.mfcc(y1,sr1)   #Computing MFCC values
librosa.display.specshow(mfcc1)

plt.subplot(2, 1, 2)
mfcc2 = librosa.feature.mfcc(y2, sr2)
librosa.display.specshow(mfcc2)

dist, _, cost, path = dtw(mfcc1.T, mfcc2.T, dist=euclidean)
print("The normalized distance between the two : ",dist)

plt.imshow(cost.T, origin='lower', cmap=plt.get_cmap('gray'), interpolation='nearest')
plt.plot(path[0], path[1], 'w')   #creating plot for DTW

plt.show()
